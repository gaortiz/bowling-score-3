package com.jobsity.challenge.factories.impl;

import com.jobsity.challenge.factories.ILastFrameCreator;
import com.jobsity.challenge.model.Frame;
import com.jobsity.challenge.model.Shot;
import org.springframework.stereotype.Service;

@Service
public class LastFrameCreator implements ILastFrameCreator {

    public Frame createLastFrame(Shot actual, Shot next, Shot afterNext) {
        return new Frame(actual, next, afterNext);
    }
}
