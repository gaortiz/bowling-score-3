package com.jobsity.challenge.factories.impl;

import com.jobsity.challenge.factories.IShotCreator;
import com.jobsity.challenge.model.Shot;
import org.springframework.stereotype.Service;

@Service
public class ShotCreator implements IShotCreator {

    public Shot createShot(String name, Integer value) {
        return new Shot(name, value);
    }
}
