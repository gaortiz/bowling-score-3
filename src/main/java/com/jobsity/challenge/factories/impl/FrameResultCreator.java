package com.jobsity.challenge.factories.impl;

import com.jobsity.challenge.factories.IFrameResultCreator;
import com.jobsity.challenge.model.FramesResult;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class FrameResultCreator implements IFrameResultCreator {

    public FramesResult createFrameResult(List<Integer> score, Map<Integer, List<String>> pinFalls, String player) {
        return new FramesResult(score, pinFalls, player);
    }
}
