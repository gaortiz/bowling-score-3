package com.jobsity.challenge.factories;

import com.jobsity.challenge.model.Shot;

public interface IShotCreator {

    Shot createShot(String name, Integer value);

}
