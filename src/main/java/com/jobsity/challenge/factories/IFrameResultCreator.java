package com.jobsity.challenge.factories;

import com.jobsity.challenge.model.FramesResult;

import java.util.List;
import java.util.Map;

public interface IFrameResultCreator {

    FramesResult createFrameResult(List<Integer> score, Map<Integer, List<String>> pinFalls, String player);

}
