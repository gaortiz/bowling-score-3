package com.jobsity.challenge;

import com.jobsity.challenge.controllers.BowlingScoreController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class App {

    public static void main(String[] args) {

        ApplicationContext ctx = SpringApplication.run(App.class, args);

        final String fileName = args[0];

        BowlingScoreController controller = (BowlingScoreController) ctx.getBean("bowlingScoreController");
        controller.calculateScore(fileName);

    }
}
