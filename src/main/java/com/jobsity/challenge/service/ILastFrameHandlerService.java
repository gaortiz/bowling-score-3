package com.jobsity.challenge.service;

import com.jobsity.challenge.model.Frame;

import java.util.List;
import java.util.Map;

public interface ILastFrameHandlerService {

    List<String> setLastFramePinFalls(Frame lastFrame);

}
