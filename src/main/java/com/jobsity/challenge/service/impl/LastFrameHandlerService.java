package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.model.Frame;
import com.jobsity.challenge.service.ILastFrameHandlerService;
import com.jobsity.challenge.utils.Constants;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class LastFrameHandlerService implements ILastFrameHandlerService {

    public List<String> setLastFramePinFalls(Frame lastFrame) {
        final int frameTotal = lastFrame.getActual().getValue() + lastFrame.getNext().getValue();
        if (frameTotal < Constants.MAX_PINFALL) {
            return Arrays.asList(lastFrame.getActual().getName(), lastFrame.getNext().getName());
        }
        return getLastFrameThreePinFalls(lastFrame);

    }

    private List<String> getLastFrameThreePinFalls(Frame lastFrame) {
        final String actualShot = lastFrame.getActual().getName();
        String firstPinFall = actualShot.equals(Constants.MAX_PINFALL_STRING) ? Constants.X : actualShot;
        String secondPinFall = getLastFrameSecondPinFall(lastFrame, firstPinFall);
        String lastPinFall = getLastFrameLastPinFall(lastFrame,firstPinFall);
        return Arrays.asList(firstPinFall, secondPinFall, lastPinFall);
    }

    private String getLastFrameSecondPinFall(Frame lastFrame, String firstPinFall) {
        final int nextShotValue = lastFrame.getNext().getValue();
        if (firstPinFall.equals(Constants.X) && nextShotValue == Constants.MAX_PINFALL) {
            return Constants.X;
        } else if (lastFrame.getActual().getValue() + nextShotValue == Constants.MAX_PINFALL && !firstPinFall.equals(Constants.X)) {
            return Constants.SLASH;
        } else {
            return lastFrame.getNext().getName();
        }
    }

    private String getLastFrameLastPinFall(Frame lastFrame, String firstPinFall) {
        final int afterNextShotValue = lastFrame.getAfterNext().getValue();
        final int frameTotal = lastFrame.getNext().getValue() + afterNextShotValue;
        if (firstPinFall.equals(Constants.X) && frameTotal == Constants.MAX_PINFALL) {
            return Constants.SLASH;
        } else if (afterNextShotValue == Constants.MAX_PINFALL) {
            return Constants.X;
        } else {
            return lastFrame.getAfterNext().getName();
        }
    }
}
