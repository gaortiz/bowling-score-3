package com.jobsity.challenge.service;

import com.jobsity.challenge.exceptions.BowlingScoreException;
import com.jobsity.challenge.model.FramesResult;

import java.util.List;

public interface IScoreCalculator {

    FramesResult calculateScore(List<String> shots, String player) throws BowlingScoreException;

}
