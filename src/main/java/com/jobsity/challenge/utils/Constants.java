package com.jobsity.challenge.utils;

public class Constants {

    public static final String X = "X";
    public static final String SLASH = "/";
    public static final String MAX_PINFALL_STRING = "10";
    public static final Integer MAX_PINFALL = 10;
    public static final Integer TOTAL_FRAMES = 10;

}
